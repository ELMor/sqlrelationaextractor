Ejemplo de uso de la utilidad:

C:\temp>java -Xms128M -Xmx256M -jar SQLRelationalExtractor.jar 
	-datadir c:\temp\prueba 
	-schema informix 
	-cargarHijos 
	-jdbc jdbc:informix-sqli:10.111.96.82:1526/clisop:INFORMIXSERVER=ifx731;user=informix;password= 
	-dsTabla pacientes 
	-dsWhere "Where numerohc between 2001000 and 2001500" 
	-encripta pacientes 
	-recrearTablas
	
	Esto crea en c:\temp\prueba ficheros txt cuyos nombres corresponden a los de las tablas que se 
	encuentran en el esquema informix de la BBDD informix referenciada por el parametro -jdbc.
	
	Se comienza con la tabla pacientes, insertando aquellos cuya numerohc est� entre 2001000 y 2001500,
	y a continuaci�n se insertan todas las tablas necesarias para que se cumpla la integridad referencial.
	
	Como adem�s se especifica -cargarHijos, se cargan todas las tablas 'Hijas' de la tabla paciente, solo
	aquellos registros cuyos padres estan entre los pacientes seleccionados. Asimismo para cada una de
	estas tablas se cargan todos los registros necesarios seg�n la integridad referencial.
	
	Como se especifica -encripta pacientes, los campos de esa tabla que sean de tipo CHAR o VARCHAR se
	sustituyen por cadenas sin sentido del tipo 'str1', manteniendo la integridad referencial en el
	caso de que alguno de esos campos sea Foreign Key de otro. En la tabla ssfsustencript se encuentran
	las relaciones entre esas cadenas (si se borra, se anonimiza la informaci�n).
 
C:\temp>java -Xms128M -Xmx256M -jar SQLRelationalExtractor.jar 
	-datadir c:\temp\prueba 
	-schema informix 
	-cargarHijos 
	-jobname informix.job
	-dsTabla otratabla 
	-dsWhere "where id=1" 
	-noCrearTablas
	
	Si a continuaci�n escribimos el comando de arriba, se a�aden los registros de la tabla 'otratabla'
	a los existentes (sin que se dupliquen).
	
	Se especifica -jobname, por lo que la metainformaci�n de la BBDD origen se lee de un fichero generado
	en una ejecuci�n anterior.
	
	Se especifica -noCrearTablas, por lo que en la BBDD 'destino' (la de hypersonic) no se vuelven a 
	crear las tablas, respetando las que estuviesen creadas en ejecuciones anteriores de la herramienta.
 