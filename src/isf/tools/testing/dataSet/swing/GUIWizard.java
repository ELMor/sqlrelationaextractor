package isf.tools.testing.dataSet.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpringLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import isf.tools.testing.dataSet.EsquemaDB;
import isf.tools.testing.dataSet.Mapeador;
import isf.tools.testing.dataSet.Procedimiento;
import isf.tools.testing.dataSet.Sustituyente;
import isf.tools.testing.dataSet.Tabla;

public class GUIWizard extends JDialog {
	private JTable tTablas;
	private JCheckBox todasLasTablasCheckBox;
	private JScrollPane spLog;
	private JTextPane log;
	private JButton conectarButton;
	private SwingLogger swingLogger=null;

	private JCheckBox cbCrearSQL;

	private JCheckBox cbRecrear;

	private JCheckBox cbNoCreartablas;

	private JCheckBox cbCrearXML;

	private JTextField tfDirectorio;

	private JButton btRun;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4972359663395486853L;

	private SpringLayout springLayout_2;


	private SpringLayout springLayout;


	private JTable tCampos;

	private JTextField tfPassword;

	private JTextField tfUser;

	private JTextField tfJdbc;

	private JTextField tfEsquema;


	private EsquemaDB schOrig = null;

	private File datadir = null;

	/**
	 * Launch the application
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			GUIWizard dialog = new GUIWizard();
			dialog.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
			dialog.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class rtRunExtraction implements Runnable {
		public void run() {
			btRun.setEnabled(false);
			Sustituyente s = null;
			datadir = new File(tfDirectorio.getText());
			String schema = tfEsquema.getText();
			boolean dumpXML = cbCrearXML.isSelected();
			/*
			Procedimiento.proc01_SaveSchemaToJOBFile(schOrig, datadir, schema,	dumpXML);
			try {
				Connection dest = DriverManager.getConnection("jdbc:hsqldb:file:"
						+ datadir + "/" + schema);
				s = new Mapeador(dest);
				s.inicializa();
				if (cbRecrear.isSelected())
					Procedimiento.proc02_DropTablasEnDest(schOrig, dest);
				if (cbRecrear.isSelected() || !cbNoCreartablas.isSelected()) {
					Procedimiento.proc03_CreaReplicaEnDEST(schOrig, dest);
				}
				if (todasLasTablasCheckBox.isSelected()) {
					Procedimiento.proc80_ddbbCompleta(schOrig, dest);
				} else {
					TableModel tmodel = tTablas.getModel();
					for (int i = 0; i < tmodel.getRowCount(); i++) {
						String tname = (String) tmodel.getValueAt(i, 0);
						Tabla tabla=schOrig.getTabla(tname);
						tabla.setSustituyente(s);
						boolean procesa = ((Boolean) tmodel.getValueAt(i, 1)).booleanValue();
						boolean encript = ((Boolean) tmodel.getValueAt(i, 3)).booleanValue();
						if(encript){
							tabla.setEncriptar();						
						}
						if (procesa) {
							String where = (String) tmodel.getValueAt(i, 2);
							if(where.equals(""))
								where="Where 1=1";
							boolean childs = ((Boolean) tmodel.getValueAt(i, 4)).booleanValue();
							Procedimiento.proc80_recursivo(schOrig, dest, tname, where, 0, "");
							if (childs)
								Procedimiento.proc90_recursivo(schOrig, dest, childs, tname, 1, "");
						}
					}
				}

				// Se procede a la encriptacion de los campos marcados
				Procedimiento.procA0_encripta(schOrig, dest);
				// Finalmente se emiten sentencias SQL Create e Insert para las
				// tablas de dest.
				FileWriter fwSQL = null;
				if (cbCrearSQL.isSelected()){
					fwSQL=new FileWriter(new File(datadir,schema+".sql"));
					Procedimiento.procB0_emitSQL(schOrig, dest, fwSQL);
					fwSQL.close();
				}
				//Procedimiento.procFF_CloseALL(schOrig, dest, null);
				btRun.setEnabled(true);
				Procedimiento.log.println("Fin de la Extraccion");
			} catch (Exception e1) {
				e1.printStackTrace();
				btRun.setEnabled(true);
			}*/
		}
		
	}
	
	public void runExtraction() {
		new Thread(new rtRunExtraction()).start();
	}

	private class rtConectar implements Runnable{
		public void run() {
			conectarButton.setEnabled(false);
			if(swingLogger==null)
				swingLogger=new SwingLogger(spLog,log);
			/*
			schOrig = new EsquemaDB(
					tfJdbc.getText(),
					tfUser.getText(),
					tfPassword.getText(),
					tfEsquema.getText(),
					swingLogger);
			tTablas.setModel(new TablasTableModel(schOrig));
			// Notoficamos a tCampos de los cambios de seleccion en su maestra
			tTablas.getSelectionModel().addListSelectionListener(
					new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent e) {
							tCampos
									.setModel(new CamposTableModel(tTablas, schOrig));
							tCampos.getColumnModel().getColumn(0)
									.setPreferredWidth(100);
							tCampos.getColumnModel().getColumn(1)
									.setPreferredWidth(100);
							tCampos.getColumnModel().getColumn(2)
									.setPreferredWidth(100);
							tCampos.getColumnModel().getColumn(3)
									.setPreferredWidth(20);
							tCampos.repaint();
						}

					});
			tTablas.getColumnModel().getColumn(0).setPreferredWidth(75);
			tTablas.getColumnModel().getColumn(1).setPreferredWidth(25);
			tTablas.getColumnModel().getColumn(2).setPreferredWidth(300);
			tTablas.getColumnModel().getColumn(3).setPreferredWidth(25);
			tTablas.getColumnModel().getColumn(4).setPreferredWidth(45);
			tTablas.repaint();
			conectarButton.setEnabled(true);
			swingLogger.println("OK, seleccione tablas en el siguiente panel.");
		*/	
		}
	}
	
	private void Conectar() throws Exception {
		rtConectar rtc=new rtConectar();
		Thread t=new Thread(rtc);
		t.start();
	}

	/**
	 * Create the dialog
	 */
	public GUIWizard() {
		super();
		getContentPane().setLayout(new BorderLayout());
		setTitle("Wizard de Extracci�n");
		setBounds(100, 100, 708, 538);

		final JTabbedPane tabbedPane = new JTabbedPane();
		getContentPane().add(tabbedPane);

		final JPanel panel = new JPanel();
		springLayout_2 = new SpringLayout();
		panel.setLayout(springLayout_2);
		tabbedPane.addTab("DDBB", null, panel, null);

		final JLabel jdbcUrlLabel = new JLabel();
		jdbcUrlLabel.setText("JDBC URL:");
		panel.add(jdbcUrlLabel);
		springLayout_2.putConstraint(SpringLayout.SOUTH, jdbcUrlLabel, 30,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, jdbcUrlLabel, 73,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, jdbcUrlLabel, 8,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, jdbcUrlLabel, 9,
				SpringLayout.WEST, panel);

		tfJdbc = new JTextField();
		tfJdbc.setText("jdbc:oracle:thin:@10.111.96.32:1521:sid1");
		panel.add(tfJdbc);
		springLayout_2.putConstraint(SpringLayout.SOUTH, tfJdbc, 50,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, tfJdbc, 479,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, tfJdbc, 30,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, tfJdbc, 9,
				SpringLayout.WEST, panel);

		final JLabel jdbcUrlLabel_1 = new JLabel();
		jdbcUrlLabel_1.setText("Usuario");
		panel.add(jdbcUrlLabel_1);
		springLayout_2.putConstraint(SpringLayout.SOUTH, jdbcUrlLabel_1, 77,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, jdbcUrlLabel_1, 73,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, jdbcUrlLabel_1, 55,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, jdbcUrlLabel_1, 9,
				SpringLayout.WEST, panel);

		tfUser = new JTextField();
		tfUser.setText("IAEGC");
		panel.add(tfUser);
		springLayout_2.putConstraint(SpringLayout.SOUTH, tfUser, 97,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, tfUser, 479,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, tfUser, 77,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, tfUser, 9,
				SpringLayout.WEST, panel);

		final JLabel jdbcUrlLabel_2 = new JLabel();
		jdbcUrlLabel_2.setText("Password");
		panel.add(jdbcUrlLabel_2);
		springLayout_2.putConstraint(SpringLayout.SOUTH, jdbcUrlLabel_2, 127,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, jdbcUrlLabel_2, 73,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, jdbcUrlLabel_2, 105,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, jdbcUrlLabel_2, 9,
				SpringLayout.WEST, panel);

		tfPassword = new JTextField();
		tfPassword.setText("IAEGC");
		panel.add(tfPassword);
		springLayout_2.putConstraint(SpringLayout.SOUTH, tfPassword, 147,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, tfPassword, 479,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, tfPassword, 127,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, tfPassword, 9,
				SpringLayout.WEST, panel);

		conectarButton = new JButton();
		conectarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Conectar();
				} catch (Exception sqle) {
					swingLogger.println(sqle.getMessage());
				}
			}
		});
		conectarButton.setText("Conectar");
		panel.add(conectarButton);
		springLayout_2.putConstraint(SpringLayout.SOUTH, conectarButton, 254,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, conectarButton, 298,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, conectarButton, 229,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, conectarButton, 188,
				SpringLayout.WEST, panel);

		final JLabel lbEsquema = new JLabel();
		lbEsquema.setText("Esquema");
		panel.add(lbEsquema);
		springLayout_2.putConstraint(SpringLayout.SOUTH, lbEsquema, 176,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, lbEsquema, 74,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, lbEsquema, 154,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, lbEsquema, 10,
				SpringLayout.WEST, panel);

		tfEsquema = new JTextField();
		tfEsquema.setText("IAEGC");
		panel.add(tfEsquema);
		springLayout_2.putConstraint(SpringLayout.SOUTH, tfEsquema, 196,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.EAST, tfEsquema, 480,
				SpringLayout.WEST, panel);
		springLayout_2.putConstraint(SpringLayout.NORTH, tfEsquema, 176,
				SpringLayout.NORTH, panel);
		springLayout_2.putConstraint(SpringLayout.WEST, tfEsquema, 10,
				SpringLayout.WEST, panel);

		final JPanel panel_1 = new JPanel();
		panel_1.setLayout(new BorderLayout());
		tabbedPane.addTab("Tablas", null, panel_1, null);

		final JPanel panel_3 = new JPanel();
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
		panel_1.add(panel_3, BorderLayout.CENTER);

		final JScrollPane scrollPane = new JScrollPane();
		panel_3.add(scrollPane);

		tTablas = new JTable();
		scrollPane.setViewportView(tTablas);

		final JScrollPane scrollPane_1 = new JScrollPane();
		panel_3.add(scrollPane_1);

		tCampos = new JTable();
		scrollPane_1.setViewportView(tCampos);

		todasLasTablasCheckBox = new JCheckBox();
		todasLasTablasCheckBox.setText("Todas Las Tablas");
		panel_1.add(todasLasTablasCheckBox, BorderLayout.SOUTH);

		final JPanel panel_2 = new JPanel();
		springLayout = new SpringLayout();
		panel_2.setLayout(springLayout);
		tabbedPane.addTab("Directorio Salida", null, panel_2, null);

		final JLabel jdbcUrlLabel_3 = new JLabel();
		jdbcUrlLabel_3.setText("Directorio de Salida");
		panel_2.add(jdbcUrlLabel_3);
		springLayout.putConstraint(SpringLayout.SOUTH, jdbcUrlLabel_3, 32,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, jdbcUrlLabel_3, 140,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, jdbcUrlLabel_3, 10,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, jdbcUrlLabel_3, 15,
				SpringLayout.WEST, panel_2);

		tfDirectorio = new JTextField();
		tfDirectorio.setText("c:/Java/WorkSpaces/31/SQLRelationalExtractor/pruebas/out");
		panel_2.add(tfDirectorio);
		springLayout.putConstraint(SpringLayout.SOUTH, tfDirectorio, 52,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, tfDirectorio, 435,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, tfDirectorio, 32,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, tfDirectorio, 15,
				SpringLayout.WEST, panel_2);

		cbCrearSQL = new JCheckBox();
		cbCrearSQL.setSelected(true);
		cbCrearSQL.setText("Crear Archivo SQL");
		panel_2.add(cbCrearSQL);
		springLayout.putConstraint(SpringLayout.SOUTH, cbCrearSQL, 80,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, cbCrearSQL, 425,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, cbCrearSQL, 60,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, cbCrearSQL, 15,
				SpringLayout.WEST, panel_2);

		cbCrearXML = new JCheckBox();
		cbCrearXML.setText("Crear Archivo XML con MetaDatos");
		panel_2.add(cbCrearXML);
		springLayout.putConstraint(SpringLayout.SOUTH, cbCrearXML, 110,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, cbCrearXML, 0,
				SpringLayout.EAST, tfDirectorio);
		springLayout.putConstraint(SpringLayout.NORTH, cbCrearXML, 90,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, cbCrearXML, 15,
				SpringLayout.WEST, panel_2);

		cbNoCreartablas = new JCheckBox();
		cbNoCreartablas.setText("No Crear otra vez las tablas en Hypersonic");
		panel_2.add(cbNoCreartablas);
		springLayout.putConstraint(SpringLayout.SOUTH, cbNoCreartablas, 140,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, cbNoCreartablas, 400,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, cbNoCreartablas, 120,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, cbNoCreartablas, 15,
				SpringLayout.WEST, panel_2);

		cbRecrear = new JCheckBox();
		cbRecrear.setText("Borrar y Crear las tablas de Hypersonic");
		panel_2.add(cbRecrear);
		springLayout.putConstraint(SpringLayout.SOUTH, cbRecrear, 170,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, cbRecrear, 410,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, cbRecrear, 150,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, cbRecrear, 15,
				SpringLayout.WEST, panel_2);

		btRun = new JButton();
		btRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Procedimiento.log=swingLogger;
				runExtraction();
			}
		});
		btRun.setText("Extraer");
		panel_2.add(btRun);
		springLayout.putConstraint(SpringLayout.SOUTH, btRun, 235,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.EAST, btRun, 290,
				SpringLayout.WEST, panel_2);
		springLayout.putConstraint(SpringLayout.NORTH, btRun, 210,
				SpringLayout.NORTH, panel_2);
		springLayout.putConstraint(SpringLayout.WEST, btRun, 175,
				SpringLayout.WEST, panel_2);

		spLog = new JScrollPane();
		spLog.setMaximumSize(new Dimension(0, 30));
		spLog.setAutoscrolls(true);
		getContentPane().add(spLog, BorderLayout.SOUTH);

		log = new JTextPane();
		log.setPreferredSize(new Dimension(0, 30));
		log.setMaximumSize(new Dimension(0, 30));
		log.setText("\n\n\n");
		log.setEditable(false);
		spLog.setViewportView(log);

		//
	}


	public JTextField getTfEsquema() {
		return tfEsquema;
	}

	public JTextField getTfJdbc() {
		return tfJdbc;
	}

	public JTextField getTfUser() {
		return tfUser;
	}

	public JTextField getTfPassword() {
		return tfPassword;
	}

	public JTable getTCampos() {
		return tCampos;
	}


	public JButton getBtRun() {
		return btRun;
	}

	public JTextField getTfDirectorio() {
		return tfDirectorio;
	}

	public JCheckBox getCbCrearXML() {
		return cbCrearXML;
	}

	public JCheckBox getCbNoCreartablas() {
		return cbNoCreartablas;
	}

	public JCheckBox getCbRecrear() {
		return cbRecrear;
	}

	public JCheckBox getCbCrearSQL() {
		return cbCrearSQL;
	}

	public JButton getConectarButton() {
		return conectarButton;
	}
	public JTextPane getLog() {
		return log;
	}
	public JScrollPane getSpLog() {
		return spLog;
	}

}
