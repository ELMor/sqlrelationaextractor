package isf.tools.testing.dataSet;

import java.io.Serializable;

public interface Sustituyente extends Serializable {
	public String sustituye(String orig);

	public void inicializa() throws Exception;

	public boolean isInicializado();
}
