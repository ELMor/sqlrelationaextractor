package isf.tools.testing.dataSet;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Environment {
	EsquemaDB schOrig = null;
	Connection dest = null;
	File datadir = null;
	String schema = null;
	String dsTabla = null;
	String dsWhere = "Where 1=1";
	String encripta = "";
	String user=null;
	String password=null;
	String propDrivers=null;
	String jdbc = null, jobname = null, datadirs;
	boolean loadChildrens = false;
	boolean loadAllChildrens = false;
	boolean crearTablas = true;
	boolean recrearTablas = false;
	boolean emitirSQL = false;
	boolean dumpXML = false;
	boolean ddbbCompleta = false;
	boolean goToGUI=true;
	RELogger log=new SystemOut();
	Connection orig=null;

	FileWriter fwSQL = null;

	public static void muestraAyuda() {
		System.out.println("Argumentos:");
		System.out.println(" [-jdbc jdbcString [-user usr -password pwd] | -jobname filename]");
		System.out .println(" [-dsTabla TABLA -dsWhere \"SQL Where\" | -ddbbCompleta]");
		System.out.println(" -datadir dir");
		System.out.println(" -schema schemaName");
		System.out.println(" -noCrearTablas");
		System.out.println(" -recrearTablas");
		System.out.println(" -cargarHijos");
		System.out.println(" -cargarTodosLosHijos");
		System.out.println(" -dumpXML");
		System.out.println(" -encripta \"TABLA,TABLA.CAMPO,...]\"");
		System.out.println(" -emitSQLInsert");
		System.out.println("\nDescripcion\n");
		System.out.println(" -datadir            :\n\tDirectorio de salida.");
		System.out.println(" -schema             :\n\tNombre del propietario de las tablas que se quieren extraer");
		System.out.println(" -jdbc               :\n\tConexion jdbc, debe inlcuir el usr/pwd");
		System.out.println(" -dsTabla            :\n\tnombre de la tabla objetivo de la extraccion.");
		System.out.println(" -dsWhere            :\n\tContiene el Where de los registros que se quieren extraer de dsTabla");
		System.out.println(" -noCrearTablas      :\n\tNo vuelve a crear las tablas de hypersonic, las complementa con los nuevos registros");
		System.out.println(" -recrearTablas      :\n\tSe hace drop y create de las tablas de hypersonic");
		System.out.println(" -cargarHijos        :\n\tCarga las tablas hijas de dsTabla");
		System.out.println(" -cargarTodosLosHijos:\n\tCarga las tablas hijas de todas las tablas que se inserten");
		System.out.println(" -jobname            :\n\tFichero <schema.job> generado en una conexion anterior, usado para esta vez");
		System.out.println(" -encripta           :\n\tlos campos especificados separados por comas, si son VARCHAR, se sustituyen en ssfsustencript.txt");
		System.out.println(" -emitSQLInsert      :\n\tCrea el archivo <schema.sql> con los insert adecuados a la extraccion");
		System.out.println(" -dumpXML            :\n\tGenera un fichero XML con la meta-informacion de la BBDD.");
		System.out.println("\nEjemplo:\n-jdbc jdbc:oracle:thin:IAEGC/IAEGC@10.111.96.32:1521:sid1 -schema IAEGC -datadir c:/temp -dsTabla IAE_PROVINCIA -cargarTodosLosHijos -recrearTablas -encripta IAE_PROVINCIA.NOMBRE");
	}

	public Environment(String arg[]) throws Exception {
		if(arg.length==0)
			return;
		// Creamos primero la lista de tablas,PKs y FKs
		for (int i = 0; i < arg.length; i++) {
			if (arg[i].equalsIgnoreCase("-jdbc"))  jdbc = arg[++i];
			else if (arg[i].equalsIgnoreCase("-jobname")) jobname = arg[++i];
			else if (arg[i].equalsIgnoreCase("-schema")) schema = arg[++i];
			else if (arg[i].equalsIgnoreCase("-datadir")) datadirs = arg[++i];
			else if (arg[i].equalsIgnoreCase("-dsTabla")) dsTabla = arg[++i];
			else if (arg[i].equalsIgnoreCase("-dsWhere")) dsWhere = arg[++i];
			else if (arg[i].equalsIgnoreCase("-cargarHijos")) loadChildrens = true;
			else if (arg[i].equalsIgnoreCase("-cargarTodosLosHijos")) loadAllChildrens = true;
			else if (arg[i].equalsIgnoreCase("-noCrearTablas")) crearTablas = false;
			else if (arg[i].equalsIgnoreCase("-recrearTablas")) recrearTablas = true;
			else if (arg[i].equalsIgnoreCase("-encripta")) encripta = arg[++i];
			else if (arg[i].equalsIgnoreCase("-emitSQLInsert")) emitirSQL = true;
			else if (arg[i].equalsIgnoreCase("-dumpXML")) dumpXML = true;
			else if (arg[i].equalsIgnoreCase("-ddbbCompleta")) ddbbCompleta = true;
			else if (arg[i].equalsIgnoreCase("-user")) user = arg[++i];
			else if (arg[i].equalsIgnoreCase("-password")) password = arg[++i];
			else if (arg[i].equalsIgnoreCase("-drivers")) propDrivers = arg[++i];
			else {
				muestraAyuda();
				System.out.println("Opcion desconocida " + arg[i]);
				System.exit(-1);
			}
		}
		goToGUI=false;
		if (schema == null) {
			muestraAyuda();
			System.out.println("-schema no es opcional..."); System.exit(-1);
		}
		if (datadirs == null) {
			muestraAyuda();
			System.out.println("-datadir no es opcional..."); System.exit(-1);
		}
		this.datadir = new File(datadirs);
		if (!this.datadir.isDirectory()) {
			muestraAyuda();
			System.out.println("-datadir no especifica un directorio..."); System.exit(-1);
		}
		if (dsTabla == null && !ddbbCompleta) {
			muestraAyuda();
			System.out.println("-dsTabla no es opcional..."); System.exit(-1);
		}
		if (dsWhere.equals("Where 1=1") && !ddbbCompleta) {
			System.out.println("ATENCION: Replicando la tabla " + dsTabla + " completa!!");
		}
		if (user == null || password==null) {
			muestraAyuda();
			System.out.println("-user y -password no son opcionales..."); System.exit(-1);
		}
	}

	public void connect() throws SQLException, Exception 
	{
		if(datadirs==null || schema==null || (jdbc==null && jobname==null) || user==null || password==null) {
			throw new SREException();
		}
		dest = DriverManager.getConnection("jdbc:h2:" + datadirs + "/" + schema);
		if (jdbc != null) {
			schOrig = new EsquemaDB(this);
		} else {
			if (jobname != null) {
				schOrig = new EsquemaDB(this,new File(datadirs + "/" + schema + ".job"));
			} else {
				muestraAyuda();
				System.out.println("Ni -jdbc ni -jobname especificado...");
				System.exit(-1);
			}
		}
		if (emitirSQL) {
			fwSQL = new FileWriter(new File(datadir, schema + ".sql"));
		}
	}

}
