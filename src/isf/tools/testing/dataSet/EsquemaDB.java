package isf.tools.testing.dataSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class EsquemaDB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1535167253485207855L;
	private Hashtable<String,Tabla> tablas = new Hashtable<>();
	private Hashtable<String,Tabla> posTablas = new Hashtable<>();
	private int numTablas=0;
	private Environment env;

	/**
	 * Conjunto de tablas
	 * 
	 * @return tablas.
	 */
	public Enumeration<String> getTablas() {
		return tablas.keys();
	}
	
	public int numTablas(){
		return tablas.size();
	}
	
	public Tabla getTablaAt(int pos){
		return posTablas.get(""+pos);
	}

	/**
	 * Obtiene la definici�n de una tabla de este esquema
	 * 
	 * @param nombre
	 *            Nombre de la tabla
	 * @return
	 */
	public Tabla getTabla(String nombre) {
		Tabla t = tablas.get(nombre);
		if (t != null) {
			Sustituyente ss = t.getSustituyente();
			if (ss != null && !ss.isInicializado())
				try {
					ss.inicializa();
				} catch (Exception e) {
					e.printStackTrace();
				}
			return t;
		}
		return creaTabla(nombre);
	}

	/**
	 * Crea la definici�n de una tabla en este esquema
	 * 
	 * @param nombre
	 *            Nombre de la tabla
	 * @return
	 */
	public Tabla creaTabla(String nombre) {
		Tabla t = new Tabla(nombre);
		tablas.put(nombre, t);
		posTablas.put(""+(numTablas++),t);
		return t;
	}

	/**
	 * Constructor partiendo del nombre del archivo grabado con 'save'
	 * 
	 * @param fname
	 *            Nombre del archivo a cargar
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public EsquemaDB(Environment e, File fname) throws Exception {
		env=e;
		env.log.println("Cargando metainformacion desde " + fname.getName());
		FileInputStream fis = new FileInputStream(fname);
		ObjectInputStream ois = new ObjectInputStream(fis);
		tablas =  (Hashtable<String, Tabla>) ois.readObject();
		env.schema = (String) ois.readObject();
		env.jdbc = (String) ois.readObject();
		env.user=(String) ois.readObject();
		env.password=(String) ois.readObject();
		ois.close();
		fis.close();
		conectar();
	}

	/**
	 * Graba los metadatos de esta conexion en un archivo
	 * 
	 * @param fname
	 *            Archivo creado usando writeobject.
	 * @throws Exception
	 */
	public void grabar(File file) throws FileNotFoundException, IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(tablas);
		oos.writeObject(env.schema);
		oos.writeObject(env.jdbc);
		oos.writeObject(env.user);
		oos.writeObject(env.password);
		oos.close();
		fos.close();
	}

	/**
	 * Constructor mediante la especificaci�n de una conexi�n jdbc hy un esquema
	 * espec�fico de esa BBDD
	 * 
	 * @param jdbc
	 *            Conexi�n JDBC, debe incluir usuario y password
	 * @param schema
	 *            Nombre del esquema a usar (propietario de las tablas)
	 */
	public EsquemaDB(Environment e) {
		env=e;
		try {
			env.log.print("Cargando todos los campos desde BBDD...");
			int count=0;

			conectar();
			DatabaseMetaData dbm = env.orig.getMetaData();
			ResultSet rs = dbm.getColumns(null, null, null, null);
			while (rs.next()) {
				String tn = rs.getString(3);
				String cn = rs.getString(4);
				int ct = rs.getInt(5);
				String ty = rs.getString(6);
				int cSize = rs.getInt(7);
				Tabla tt = getTabla(tn);
				tt.addCampo(new Campo(tn, cn, ct, ty, cSize));
				count++;
			}
			rs.close();
			env.log.println("Campos obtenidos ("+count+")");
			count=0;
			env.log.println("Cargando PKs desde BBDD...");
			for (Enumeration<String> enu = tablas.keys(); enu.hasMoreElements();) {
				String tablaNombre = enu.nextElement();
				rs = dbm.getPrimaryKeys(null, env.schema, tablaNombre);
				Tabla tt = null;
				count++;
				while (rs.next()) {
					String tn = rs.getString(3);
					String cn = rs.getString(4);
					int po = rs.getInt(5);
					tt = getTabla(tn);
					Campo cc = tt.getCampo(cn);
					tt.addCampoPK(po, cc);
				}
				rs.close();
				if (tt != null)
					tt.getListPKFields();
				//else
					//env.log.println("\tATENCION:" + tablaNombre
							//+ " no tiene PK!!!!");
			}
			env.log.println("OK:("+count+"/"+numTablas()+")");
			count=0;
			env.log.print("Cargando FKs desde BBDD...");			
			for(Enumeration<String> t1=tablas.keys();t1.hasMoreElements();) {
				rs = dbm.getImportedKeys(null, env.schema, t1.nextElement());
				while (rs.next()) {
					count++;
					String pktn = rs.getString(3);
					String pkcn = rs.getString(4);
					String fktn = rs.getString(7);
					String fkcn = rs.getString(8);
					short posi = rs.getShort(9);
					String relname = rs.getString(12);
					getTabla(fktn).addImportedPK(relname, fkcn, posi, getTabla(pktn), pkcn);
					if(count%50==0) 
						System.out.print(".");
				}
			}
			quitarTablasBasura();
			env.log.println("OK:("+count+")");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void conectar() throws SQLException {
		if(env.user!=null) {
			env.orig= DriverManager.getConnection(env.jdbc,env.user,env.password);
		}else {
			env.orig = DriverManager.getConnection(env.jdbc);
		}
		System.out.println("Connected");
	}

	private void quitarTablasBasura() {
		tablas.remove("none");
		tablas.remove("PLAN_TABLE");
		tablas.remove("EXCEPTIONS");
		tablas.remove("sysreferences");
		tablas.remove("sysdistrib");
		tablas.remove("sysfragments");

		tablas.remove("sysviews");
		tablas.remove("sysdefaults");
		tablas.remove("sysprocedures");
		tablas.remove("sysviolations");
		tablas.remove("systables");
		tablas.remove("systriggers");
		tablas.remove("sysprocbody");
		tablas.remove("sysroleauth");
		tablas.remove("systrigbody");
		tablas.remove("syssyntable");

		tablas.remove("syscoldepend");
		tablas.remove("sysobjstate");
		tablas.remove("systabauth");
		tablas.remove("sysroleauth");
		tablas.remove("sysprocplan");
		tablas.remove("sysopclstr");
		tablas.remove("syschecks");
		tablas.remove("syscolumns");
		tablas.remove("syscolauth");
		tablas.remove("sysprocauth");
		tablas.remove("sysusers");
		tablas.remove("sysdepend");
		tablas.remove("syssynonyms");
		tablas.remove("sysblobs");
		tablas.remove("sysfragauth");
		tablas.remove("sysindexes");
		tablas.remove("sysconstraints");

	}

	/**
	 * Muestra en pantalla metainformaci�n en XML acerca de esta conexi�n.
	 * 
	 */
	public void dump(PrintStream out) {
		out.print("<Schema nombre=\"" + env.schema + "\" url=\"" + env.jdbc
				+ "\">\n");
		for (Enumeration<String> k = tablas.keys(); k.hasMoreElements();) {
			Tabla t = (Tabla) tablas.get(k.nextElement());
			out.print(" <tabla nombre=\"" + t.nombre + "\">\n");
			for (Enumeration<String> f = t.getCampos().keys(); f.hasMoreElements();) {
				Campo c = (Campo) t.getCampo(f.nextElement());
				out.print("  <campo");
				out.print(" nombre=\"" + c.nombre + "\"");
				int pkPos = t.pkPosition(c);
				if (pkPos != 0)
					out.print(" pkPos=\"" + pkPos + "\"");
				out.print(" tipo=\"" + c.tipo + "\"");
				out.print("/>\n");
			}
			for (Enumeration<String> f = t.fkf.keys(); f.hasMoreElements();) {
				String fk = (String) f.nextElement();
				out.print("  <foreignKey nombre=\"" + fk + "\">\n");
				Vector<Campo> v1 = t.fkf.get(fk);
				for (int i = 0; i < v1.size(); i += 2) {
					Campo c1 = (Campo) (v1.get(i + 0));
					Campo c2 = (Campo) (v1.get(i + 1));
					out.print("   <Campo nombre=\"" + c1.nombre + "\"");
					out.print(" pos=\"" + ((i / 2) + 1) + "\"");
					out.print(" refTabla=\"" + c2.tabla + "\"");
					out.print(" refCampo=\"" + c2.nombre + "\"/>\n");
				}
				out.print("  </foreignKey>\n");
			}
			out.print(" </tabla>\n");
		}
		out.print("</Schema>\n");
	}

	/**
	 * Replica este esquema en otra conexion distinta
	 * 
	 * @param dest
	 *            conexion destino
	 * @param withdata
	 *            Si se incluyen tambi�n los registros
	 * @throws SQLException
	 */
	public void replica(Connection dest, boolean withdata) throws SQLException,
			IOException {
		for (Enumeration<String> tabEnum = tablas.keys(); tabEnum.hasMoreElements();) {
			Tabla tab = getTabla((String) tabEnum.nextElement());
			tab.create(dest);
			if (withdata)
				tab.replica(this, dest, "");
		}
	}

	public void dropTablas(Connection dest) {
		for (Enumeration<String> tabEnum = tablas.keys(); tabEnum.hasMoreElements();)
			getTabla((String) tabEnum.nextElement()).drop(dest);
	}

	public Connection getConexion() {
		return env.orig;
	}
}
