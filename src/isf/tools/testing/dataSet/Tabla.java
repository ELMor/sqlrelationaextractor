package isf.tools.testing.dataSet;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class Tabla implements Serializable {
	private static final long serialVersionUID = 7471495635470471827L;

	String nombre;
	String[] camposOrdenados = null;
	Sustituyente sust = null;
	private Hashtable<String,Campo> campos = new Hashtable<>();
	Vector<Campo> pk = new Vector<>();
	Hashtable<String,Vector<Campo>> fkf = new Hashtable<>();
	Hashtable<String, Tabla> fkb = new Hashtable<>();
	String sqlSelectFields = null;
	String sqlInsertFields = null;
	
	protected Tabla(String nombre) {
		this.nombre = nombre;
	}

	public Hashtable<String,Campo> getCampos(){
		return campos;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public void setSustituyente(Sustituyente s) {
		sust = s;
	}

	public void setEncriptar(){
		for(Enumeration<Campo> ei=campos.elements();ei.hasMoreElements();){
			(ei.nextElement()).setEncriptar(true);
		}
	}
	
	public Sustituyente getSustituyente() {
		return sust;
	}

	public void addCampo(Campo c) {
		campos.put(c.nombre, c);
		buildSQLs();
	}

	/**
	 * marca un campo como PK en una posicion determinada
	 * 
	 * @param pos
	 *            Posicion del campo
	 * @param c
	 *            Campo perteneciente al Primary Key
	 */
	public void addCampoPK(int pos, Campo c) {
		int posInVector = pos - 1;
		while (pk.size() < posInVector)
			pk.add(new Campo("none", "none", -1, "none", -1));
		pk.add(posInVector, c);
	}

	/**
	 * Obtiene la posicion en la PK del campo c
	 * 
	 * @param c
	 * @return posicion del campo
	 */
	public int pkPosition(Campo c) {
		int ret = 0;
		for (int i = 0; ret == 0 && i < pk.size(); i++) {
			Campo c2 = (Campo) pk.get(i);
			if (c.nombre.equalsIgnoreCase(c2.nombre))
				ret = 1 + i;
		}
		return ret;
	}

	/**
	 * Obtiene el campo de nombre 'nombre'
	 * 
	 * @param nombre
	 * @return
	 */
	public Campo getCampo(String nombre) {
		return campos.get(nombre);
	}

	/**
	 * Obtiene el Primary Key completo
	 * 
	 * @return Vector que contiene los campos
	 */
	public Vector<Campo> getPK() {
		return pk;
	}

	/**
	 * Retorna una enumeracion de los campos de esa tabla
	 * 
	 * @return Campos
	 */
	public Enumeration<Campo> getEnumCampos() {
		return campos.elements();
	}

	/**
	 * Obtiene lista de campos PK separada por comas
	 * 
	 * @return Lista de pk
	 */
	public String getListPKFields() {
		StringBuffer ret = new StringBuffer();
		for (int i = 0; i < pk.size(); i++) {
			Campo c = (Campo) (pk.get(i));
			if (c.nombre.equals("none")) {
				pk.remove(i--);
			} else {
				if (ret.length() == 0) {
					ret.append(c.nombre);
				} else {
					ret.append(",").append(c.nombre);
				}
			}
		}
		return ret.toString();
	}

	/**
	 * Procesa el hecho de que el campo c1 de esta tabla, mediante la relacion
	 * nombreRel, es un foreign key del campo maestro c2 de la tabla maestra
	 * tablaMaestra.
	 * 
	 * @param nombreRel
	 *            nombre de la relacion
	 * @param c1
	 *            campo local
	 * @param pos
	 *            posicion en la relacion (constraint)
	 * @param tablaMaestra
	 *            tabla maestra referenciada
	 * @param c2
	 *            campo referenciado en la tabla maestra
	 */
	public void addImportedPK(String nombreRel, String c1, int pos,
			Tabla tablaMaestra, String c2) {
		Vector<Campo> v =  fkf.get(nombreRel);
		if (v == null)
			v = new Vector<Campo>();
		Campo cc1 = getCampo(c1);
		v.add(2 * (pos - 1), cc1);
		Campo cc2 = tablaMaestra.getCampo(c2);
		v.add(2 * (pos - 1) + 1, cc2);
		// A�adimos informacion a las tablas forward y backward
		fkf.put(nombreRel, v);
		tablaMaestra.fkb.put(nombreRel, this);
		// Y alos campos tambien, por si se encriptan...
		cc1.addCampoFK(cc2);
		cc2.addCampoBK(cc1);
	}

	/**
	 * Obtiene un hashtable con los ForeignKeys de esta tabla
	 * 
	 * @return Hashtable(String::nombreRel,Vector::[campoLocal,campoMaestro])
	 */
	public Hashtable<String,Vector<Campo>> getFKs() {
		return fkf;
	}

	/**
	 * Lista de campos propios que componen la constraint
	 * 
	 * @param fkName
	 * @return lista de campos separados por comas
	 */
	public String getFKOwnFields(String fkName) {
		return getFKOwnFields(fkf.get(fkName));
	}

	/**
	 * Lista de campos propios que componen la constraint
	 * 
	 * @param v
	 *            Vector que contiene los campos locales y los referenciados
	 * @return lista de campos separados por comas
	 */
	public String getFKOwnFields(Vector<Campo> v) {
		String ret = "";
		boolean first = true;
		for (int i = 0; i < v.size(); i += 2) {
			Campo c = (Campo) v.get(i);
			if (first) {
				ret += c.nombre;
			} else {
				first = false;
				ret += "," + c.nombre;
			}
		}
		return ret;
	}

	public String getFKForeignTable(Vector<Campo> v) {
		Campo c =  v.get(1);
		return c.tabla;
	}

	public String getFKForeignTable(String fkName) {
		return getFKForeignTable( fkf.get(fkName));
	}

	public String getFKForeignFields(String fkName) {
		return getFKForeignFields(fkf.get(fkName));
	}

	public String getFKForeignFields(Vector<Campo> v) {
		String ret = "";
		boolean first = true;
		for (int i = 1; i < v.size(); i += 2) {
			Campo c = (Campo) v.get(i);
			if (first) {
				ret += c.nombre;
			} else {
				first = false;
				ret += "," + c.nombre;
			}
		}
		return ret;
	}

	private void buildSQLs() {
		sqlSelectFields = "";
		sqlInsertFields = "";
		boolean first = true;
		camposOrdenados = new String[campos.size()];
		int i = 0;
		for (Enumeration<String> camEnum = campos.keys(); camEnum.hasMoreElements();) {
			camposOrdenados[i] = (String) camEnum.nextElement();
			if (first) {
				sqlSelectFields += camposOrdenados[i];
				sqlInsertFields = "?";
				first = false;
			} else {
				sqlSelectFields += "," + camposOrdenados[i];
				sqlInsertFields += ",?";
			}
			i++;
		}
	}

	public String getCampoAt(int pos){
		if(pos>=0 && pos<camposOrdenados.length)
			return camposOrdenados[pos];
		return null;
	}
	
	/**
	 * Campos separados por comas
	 * 
	 * @return Campos separados por comas
	 */
	public String getSQLFields() {
		return sqlSelectFields;
	}

	/**
	 * Cadena de interrogaciones separadas por comas (tantas como campos tenga
	 * la tabla)
	 * 
	 * @return Cadena de interrogaciones separadas por comas (tantas como campos
	 *         tenga la tabla)
	 */
	public String getInsertFields() {
		return sqlInsertFields;
	}

	/**
	 * Construye select de los campos
	 * 
	 * @return 'select [campo,campo,...] from [tabla]'
	 */
	public String getSQLSelect() {
		return "select " + getSQLFields() + " from " + nombre;
	}

	/**
	 * Construye insert de los campos en la tabla
	 * 
	 * @return 'insert into [tabla]([campo1,campo2,...]) values
	 *         ([valor1,valor2,...])'
	 */
	public String getSQLInsert() {
		return "insert into " + nombre + "(" + getSQLFields() + ") "
				+ "values(" + getInsertFields() + ")";
	}

	/**
	 * Tama�o del campo
	 * 
	 * @param c
	 *            Campo
	 * @return cadena que complementa el campo
	 */
	private String sizeOfField(Campo c) {
		String ret = "";
		if (c.tipo.equalsIgnoreCase("varchar")
				|| c.tipo.equalsIgnoreCase("char"))
			ret = "(" + c.size + ")";
		return ret;
	}

	/**
	 * Algunos tipos no estan soportados por hypersonic SQL
	 * 
	 * @param c
	 *            Campo a comprobar
	 * @return nuevo tipo mapeado equivalente.
	 */
	private String tipoMapeado(Campo c) {
		if (c.tipo.equalsIgnoreCase("money"))
			return "NUMERIC";
		if (c.tipo.equalsIgnoreCase("text"))
			return "VARCHAR";
		if (c.tipo.equalsIgnoreCase("VARCHAR2"))
			return "VARCHAR";
		if (c.tipo.equalsIgnoreCase("ROWID"))
			return "VARCHAR(60)";
		if (c.tipo.equalsIgnoreCase("NUMBER"))
			return "NUMERIC";
		if (c.tipo.equalsIgnoreCase("serial"))
			return "NUMERIC";
		if (c.tipo.equalsIgnoreCase("int"))
			return "NUMERIC";
		if (c.tipo.equalsIgnoreCase("byte"))
			return "NUMERIC";
		if (c.tipo.equalsIgnoreCase("smallfloat"))
			return "float";
		return c.tipo;
	}

	/**
	 * Obtiene SQL para crear esta tabla
	 * 
	 * @param opt
	 *            En hypersonic se pueden crear tablas 'create
	 *            [cached|memory|...] table'
	 * @return sql con el create
	 */
	public String getSQLCreate(String opt) {
		boolean firstField = true;
		String sql = "create " + opt + " table " + nombre + "(";
		for (Enumeration<String> f = campos.keys(); f.hasMoreElements();) {
			String fname = (String) f.nextElement();
			Campo c = (Campo) campos.get(fname);
			if (!firstField)
				sql += ", ";
			else
				firstField = false;
			String szf = sizeOfField(c);
			String tip = tipoMapeado(c);
			sql += ""+ c.nombre + " " + tip + szf;
		}
		sql += ") ";
		return sql;
	}

	/**
	 * Crea esta tabla en la BBDD representada por la conexion conn
	 * 
	 * @param conn
	 *            Conexion JDBC
	 * @throws SQLException
	 * @throws IOException
	 */
	public void create(Connection conn) throws  IOException {
		String sql = getSQLCreate("cached");
		try {
			Statement st = conn.createStatement();
			st.execute(sql);
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("SQL:"+sql);
			System.exit(-1);
		}
	}

	/**
	 * Replica esta tabla
	 * 
	 * @param dborig
	 * @param dest
	 * @param sqlWhere
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public int replica(EsquemaDB dborig, Connection dest, String sqlWhere)
			throws SQLException, IOException {
		Statement stSelect = dborig.getConexion().createStatement();
		ResultSet rsSelect;
		try {
			rsSelect = stSelect.executeQuery(getSQLSelect() + " " + sqlWhere);
		} catch (SQLException e) {
			System.out.println("Error ejecutando:" + getSQLSelect() + " "
					+ sqlWhere);
			throw e;
		}
		dest.setAutoCommit(false);
		PreparedStatement psInsert = dest.prepareStatement(getSQLInsert());
		int returnCount = 0;
		while (rsSelect.next()) {
			for (int i = 1; i <= campos.size(); i++)
				psInsert.setObject(i, rsSelect.getObject(i));
			psInsert.execute();
			returnCount += psInsert.getUpdateCount();
			dest.commit();
		}
		rsSelect.close();
		stSelect.close();
		psInsert.close();
		return returnCount;
	}

	/**
	 * Borra esta tabla en la RDBMS c
	 * 
	 * @param c
	 */
	public void drop(Connection c) {
		try {
			Statement stSelect = c.createStatement();
			stSelect.execute("drop table " + nombre);
			stSelect.close();
		} catch (SQLException e) {
		}
	}

	/**
	 * 
	 * @param inc
	 * @param dest
	 * @return
	 * @throws SQLException
	 */
	public String niCond(boolean inc, Connection dest)
			throws SQLException {
		String pkList = getListPKFields();
		return innerGroupCond(pkList, pkList, inc, dest).toString();
	}

	/**
	 * Construye una condicion del WHERE de una clausula SELECT del tipo
	 * (fkList) [not] in ((valores),...) La extraccion de valores se realiza
	 * mediante el select select distinct pkList from this.tabla
	 * 
	 * @param pkList
	 *            Campos seleccionados de this.nombre para construir los valores
	 * @param fkList
	 *            Campos que se van a comrpobar (fkList) not in
	 *            ((val1),(val2),...)
	 * @param inc
	 *            true si 'in', false si 'not in'
	 * @param dest
	 *            Conexion para obtener los valores
	 * @return Condicion (fkList) [not] in ((valor1),(valor2),...)
	 * @throws SQLException
	 */
	public StringBuffer innerGroupCond(String pkList, String fkList,
			boolean inc, Connection dest) throws SQLException {
		Statement st = dest.createStatement();
		ResultSet rs = st.executeQuery("Select distinct " + pkList + " from " + nombre);
		ResultSetMetaData rsmd = rs.getMetaData();
		int sz = rsmd.getColumnCount();
		StringBuffer sb = new StringBuffer();
		boolean firstGroup = true;
		int maxGroupCount = 0;
		while (rs.next()) {
			if (!firstGroup) {
				sb.append(",");
			} else {
				firstGroup = false;
				if (sb.length() > 0)
					sb.append(" or ");
				sb.append("(").append(fkList).append(")");
				if (inc)
					sb.append(" in (");
				else
					sb.append(" not in (");
			}
			if (sz > 1)
				sb.append("(");
			for (int cn = 1; cn <= sz; cn++) {
				if (cn > 1)
					sb.append(",");
				switch (rsmd.getColumnType(cn)) {
				case Types.CHAR:
				case Types.VARCHAR:
					sb.append("'").append(rs.getString(cn).replace('\'','`')).append("'");
					break;
				default:
					sb.append(rs.getObject(cn).toString());
					break;
				}
			}
			if (sz > 1)
				sb.append(")");
			if (++maxGroupCount > 255) {
				sb.append(")");
				firstGroup = true;
				maxGroupCount=0;
			}
		}
		return sb.length() == 0 ? sb.append("1=1") : sb.append(")");
	}

	/**
	 * Crea los inserts necesarios para construir esta tabla seg�n consta en la
	 * BBDD representada por conn
	 * 
	 * @param conn
	 *            Conexion
	 * @param wr
	 *            Writer donde emitir los inserts.
	 * @throws Exception
	 */
	public void dumpSQL(Connection conn, Writer wr) throws Exception {
		if (nombre.equals("ssfsustencript"))
			return;
		wr.write(getSQLCreate(""));
		wr.write(";\n");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(getSQLSelect());
		ResultSetMetaData rsmd = rs.getMetaData();
		int sz = rsmd.getColumnCount();
		while (rs.next()) {
			wr.write("insert into " + nombre + "(" + getSQLFields()
					+ ")\n\tvalues(");
			for (int cn = 1; cn <= sz; cn++) {
				if (cn > 1)
					wr.write(",");
				Object getter = rs.getObject(cn);
				switch (rsmd.getColumnType(cn)) {
				case Types.CHAR:
				case Types.VARCHAR:
				case Types.DATE:
					wr.write(getter == null ? "null" : "'" + getter.toString()
							+ "'");
					break;
				default:
					wr.write(getter == null ? "null" : getter.toString());
					break;
				}
			}
			wr.write(");\n");
		}
		rs.close();
		st.close();
	}

	/**
	 * Encripta los campos de tipo CHAR o VARCHAR que contenga esta tabla
	 * 
	 * @param conn
	 *            RDBMS
	 * @throws SQLException
	 */
	public void encripta(Connection conn, RELogger re) throws SQLException {
		for (Enumeration<String> ec = campos.keys(); ec.hasMoreElements();) {
			Campo c =  campos.get(ec.nextElement());
			if (!c.encriptar.booleanValue())
				continue;
			switch (c.itipo) {
			case Types.CHAR:
			case Types.VARCHAR:
				c.encripta(conn, sust, re);
				break;
			default:
				continue;
			}
		}
	}

	/**
	 * Crea una sql que crea la pk de esta tabla
	 * 
	 * @param wr
	 *            Writer donde se saca la sql
	 * @throws IOException
	 */
	public void dumpSQLPKConstraint(Writer wr) throws IOException {
		if (pk != null && pk.size() > 0) {
			wr.write("Alter table " + nombre + " add primary key ("
					+ getListPKFields() + ")\n");
		}
	}

	/**
	 * Crea las constraints (FKs) de esta tabla
	 * 
	 * @param wr
	 *            donde se escriben las constraints
	 * @throws IOException
	 */
	public void dumpSQLConstraints(Writer wr) throws IOException {
		for (Enumeration<String> efk = fkf.keys(); efk.hasMoreElements();) {
			String nombreRel = (String) efk.nextElement();
			Vector<Campo> v = fkf.get(nombreRel);
			wr.write("Alter table " + nombre + " add constraint " + nombreRel
					+ " foreign key (");
			for (int i = 0; i < v.size(); i += 2) {
				if (i > 0)
					wr.write(",");
				wr.write(((Campo) v.get(i)).nombre);
			}
			wr.write(") references ");
			for (int i = 1; i < v.size(); i += 2) {
				Campo c = (Campo) v.get(i);
				if (i > 1) {
					wr.write(",");
				} else {
					wr.write(c.tabla + "(");
				}
				wr.write(c.nombre);
			}
			wr.write(");\n");
		}
	}

}