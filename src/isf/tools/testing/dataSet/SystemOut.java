package isf.tools.testing.dataSet;

public class SystemOut implements RELogger {

	public void print(String msg) {
		System.out.print(msg);
	}

	public void println(String msg) {
		System.out.println(msg);
	}

	public void flush() {
		System.out.flush();
	}
}
