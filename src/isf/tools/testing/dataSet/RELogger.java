package isf.tools.testing.dataSet;

public interface RELogger {
	public void print(String msg);
	public void println(String msg);
	public void flush();
}
