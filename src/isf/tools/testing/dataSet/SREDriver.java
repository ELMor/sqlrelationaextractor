package isf.tools.testing.dataSet;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class SREDriver implements Driver {
	private Driver d;
	SREDriver(Driver dd){ d=dd; }
	
	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		return d.connect(url, info);
	}

	@Override
	public boolean acceptsURL(String url) throws SQLException {
		return d.acceptsURL(url);
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
		return d.getPropertyInfo(url, info);
	}

	@Override
	public int getMajorVersion() {
		return d.getMajorVersion();
	}

	@Override
	public int getMinorVersion() {
		return d.getMinorVersion();
	}

	@Override
	public boolean jdbcCompliant() {
		return d.jdbcCompliant();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return d.getParentLogger();
	}

}
