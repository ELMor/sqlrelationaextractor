package isf.tools.testing.dataSet;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

public class Campo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -243252496475596400L;
	protected String tabla, nombre, tipo;
	protected int size, itipo;
	protected Boolean encriptar = new Boolean(false);
	protected Vector<Campo> camposFK = new Vector<>();
	protected Vector<Campo> camposBK = new Vector<>();
	protected transient boolean encriptado = false;
	
	public static void get(String tabla, String nombre) {
		// return (Campo)Tabla.get(tabla).campos.get(nombre);
	}

	private static Hashtable<String, String> nmaps=new Hashtable<>();
	static {
		nmaps.put("select", "select_");
		nmaps.put("type", "type_");
		nmaps.put("sysname", "nvarchar(128)");
	}
	private static String repl(String n) {
		String ret=nmaps.get(n.toLowerCase());
		return ret == null ? n : ret;
	}
	
	public Campo(String t, String n, int it, String st, int siz) {
		tabla = t;
		n=repl(n);

		nombre = n;
		itipo = it;
		size = siz;
		tipo = st;
	}

	public void setEncriptar(boolean val) {
		encriptar=new Boolean(val);
	}
	
	public Boolean getEncriptar(){
		return encriptar;
	}
	
	public void addCampoFK(Campo c) {
		camposFK.add(c);
	}

	public void addCampoBK(Campo c) {
		camposBK.add(c);
	}

	public void encripta(Connection conn, Sustituyente sust, RELogger re)
			throws SQLException {
		if(encriptado)
			return;
		re.println("Encriptando " + tabla + "." + nombre);
		encriptado = true;
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select distinct " + nombre + " from "
				+ tabla);
		while (rs.next()) {
			String actual = rs.getString(1);
			if (actual != null) {
				st.execute("update " + tabla + " set " + nombre + "='"
						+ sust.sustituye(actual) + "'");
			}
		}
		for (int i = 0; i < camposFK.size(); i++) {
			camposFK.get(i).encripta(conn, sust, re);
		}
		for (int i = 0; i < camposBK.size(); i++)
			camposBK.get(i).encripta(conn, sust, re);
	}

	public String getNombreTipo(){
		return tipo+"("+size+")";
	}
}
